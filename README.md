## Hairstyle Advisor

Welcome to Hairstyle Advisor, where we help you find the perfect hairstyle for any occasion and outfit. Follow the steps below to discover a hairstyle that suits you best.

**Introduction**

Hairstyle Advisor is a tool designed to assist users in choosing the ideal hairstyle based on their dress and the occasion they are attending.It also helps in sort the options of outfits for the occasion.

**Features**

- **Outfit Selection**: Choose among wide range of dressing options.
- **Occasion Selection**: Select the occasion you are attending, such as weddings, parties, casual outings, and more.
- **Tailored Recommendations**: Receive personalized hairstyle suggestions that complement your chosen occasion and outfit.

**Usage**
Getting Started-Follow these simple steps to get started with Hairstyle Advisor.
- Choose the occasion you are attending from the available options.
- Select your dress type.
- View personalized hairstyle recommendations tailored to your dress and occasion.
- Experiment with different styles to find the perfect one for you.

## Dependencies
List of software, libraries, or tools that this project depends on
- **Python:** 3.6 or higher
- **Pillow:** 10.2.0
- **pip:** 24.0
- **tk** 0.1.0
- **DB Browser for SQLite:** 3.12.2


## Installation

To set up the project locally, follow these steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/22b01a4271/project.git
    ```

2. Navigate to the project directory:

    ```bash
    cd project
    ```

3. Install the required dependencies:

    ```bash
    python -m pip install -r requirements.txt
    ```

4. (Optional) Download and install DB Browser for SQLite from [the official website](https://sqlitebrowser.org/).

## Getting Started

Now the project is set up, you can run it with the following command:

```bash
python main.py

