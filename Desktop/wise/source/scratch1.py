import tkinter as tk
from PIL import Image, ImageTk
import sqlite3

def initialize_database():
    try:
        conn = sqlite3.connect('new_haistyles.db')  # Change the database name
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS new_hairstyles (
                        occasion TEXT NOT NULL,
                        button_name TEXT NOT NULL,
                        image TEXT NOT NULL
                     )''')

        occasions_data = [
            ("Wedding", [
                ("Saree", ["w1.jpeg", "w2.jpeg", "w3.jpeg", "w4.jpeg", "w5.jpeg", "w6.jpeg", "w7.jpeg", "w8.jpeg", "w9.jpeg"]),
                ("Lehenga", ["w11.jpeg", "w12.jpeg", "w13.jpeg", "w14.jpeg", "w15.jpeg", "w16.jpeg", "w17.jpeg", "w18.jpeg", "w19.jpeg"]),
                ("Half saree", ["w21.jpeg", "w22.jpeg", "w23.jpeg", "w24.jpeg", "w25.jpeg", "w26.jpeg", "w27.jpeg", "w28.jpeg", "w29.jpeg"]),
                ("Long frock", ["w31.jpeg", "w32.jpeg", "w33.jpeg", "w34.jpeg", "w35.jpeg", "w36.jpeg", "w37.jpeg", "w38.jpeg", "w39.jpeg"]),
                ("Anarkali", ["w41.jpeg", "w42.jpeg", "w43.jpeg", "w44.jpeg", "w45.jpeg", "w46.jpeg", "w47.jpeg", "w48.jpeg", "w49.jpeg"])
            ]),
            ("Get Togethers", [
                ("Jeans", ["g1.jpeg", "g2.jpeg", "g3.jpeg", "g4.jpeg", "g5.jpeg", "g6.jpeg", "g7.jpeg", "g8.jpeg", "g9.jpeg"]),
                ("Simple outfits", ["g11.jpeg", "g12.jpeg", "g13.jpeg", "g14.jpeg", "g15.jpeg", "g16.jpeg", "g17.jpeg", "g18.jpeg", "g19.jpeg"]),
                ("Flowy maxi dress", ["g21.jpeg", "g22.jpeg", "g23.jpeg", "g24.jpeg", "g25.jpeg", "g26.jpeg", "g27.jpeg", "g28.jpeg", "g29.jpeg"]),
                ("Layered", ["g31.jpeg", "g32.jpeg", "g33.jpeg", "g34.jpeg", "g35.jpeg", "g36.jpeg", "g37.jpeg", "g38.jpeg", "g39.jpeg"]),
                ("Jumpsuit", ["g41.jpeg", "g42.jpeg", "g43.jpeg", "g44.jpeg", "g45.jpeg", "g46.jpeg", "g47.jpeg", "g48.jpeg", "g49.jpeg"])
            ]),
            ("Partys", [
                ("Cocktail dress", ["p1.jpeg", "p2.jpeg", "p3.jpeg", "p4.jpeg", "p5.jpeg", "p6.jpeg", "p7.jpeg", "p8.jpeg", "p9.jpeg"]),
                ("Metallic dress", ["p11.jpeg", "p12.jpeg", "p13.jpeg", "p14.jpeg", "p15.jpeg", "p16.jpeg", "p17.jpeg", "p18.jpeg", "p19.jpeg"]),
                ("Jeans", ["p31.jpeg", "p32.jpeg", "p33.jpeg", "p34.jpeg", "p35.jpeg", "p36.jpeg", "p37.jpeg", "p38.jpeg", "p39.jpeg"])
            ]),
            ("Festivals", [
                ("Long frock", ["f1.jpeg", "f2.jpeg", "f3.jpeg", "f4.jpeg", "f5.jpeg", "f6.jpeg", "f7.jpeg", "f8.jpeg", "f9.jpeg"]),
                ("Jeans", ["f11.jpeg", "f12.jpeg", "f13.jpeg", "f14.jpeg", "f15.jpeg", "f16.jpeg", "f17.jpeg", "f18.jpeg", "f19.jpeg"]),
                ("Anarkali", ["f21.jpeg", "f22.jpeg", "f23.jpeg", "f24.jpeg", "f25.jpeg", "f26.jpeg", "f27.jpeg", "f28.jpeg", "f29.jpeg"]),
                ("Kurtis", ["f31.jpeg", "f32.jpeg", "f33.jpeg", "f34.jpeg", "f35.jpeg", "f36.jpeg", "f37.jpeg", "f38.jpeg", "f39.jpeg"]),
                ("Saree", ["f41.jpeg", "f42.jpeg", "f43.jpeg", "f44.jpeg", "f45.jpeg", "f46.jpeg", "f47.jpeg", "f48.jpeg", "f49.jpeg"])
            ]),
            ("Casual Wear", [
                ("Jeans", ["c1.jpg", "c2.jpg", "c3.jpg", "c4.jpg", "c5.jpg", "c6.jpg", "c7.jpg", "c8.jpg", "c9.jpg"]),
                ("Kurtis", ["c11.jpg", "c12.jpg", "c13.jpg", "c14.jpg", "c15.jpg", "c16.jpg", "c17.jpg", "c18.jpg", "c19.jpg"]),
                ("Dresses", ["c21.jpg", "c22.jpg", "c23.jpg", "c24.jpg", "c25.jpg", "c26.jpg", "c27.jpg", "c28.jpg", "c29.jpg"])
            ]),
            ("Sports", [
                ("Shorts", ["s1.jpg", "s2.jpg", "s3.jpg", "s4.jpg", "s5.jpg", "s6.jpg", "s7.jpg", "s8.jpg", "s9.jpg"]),
                ("Leggings", ["s11.jpg", "s12.jpg", "s13.jpg", "s14.jpg", "s15.jpg", "s16.jpg", "s17.jpg", "s18.jpg", "s19.jpg"]),
                ("Training outfits", ["s21.jpg", "s22.jpg", "s23.jpg", "s24.jpg", "s25.jpg", "s26.jpg", "s27.jpg", "s28.jpg", "s29.jpg"])
            ]),
            ("Summer Styles", [
                ("Jump suit", ["su1.jpeg", "su2.jpeg", "su3.jpeg", "su4.jpeg", "su5.jpeg", "su6.jpeg", "su7.jpeg", "su8.jpeg", "su9.jpeg"]),
                ("Frocks", ["su11.jpeg", "su12.jpeg", "su13.jpeg", "su14.jpeg", "su15.jpeg", "su16.jpeg", "su17.jpeg", "su18.jpeg", "su19.jpeg"]),
                ("Flowy dresses", ["su21.jpeg", "su22.jpeg", "su23.jpeg", "su24.jpeg", "su25.jpeg", "su26.jpeg", "su27.jpeg", "su28.jpeg", "su29.jpeg"]),
                ("Maxi floral dresses", ["su31.jpeg", "su32.jpeg", "su33.jpeg", "su34.jpeg", "su35.jpeg", "su36.jpeg", "su37.jpeg", "su38.jpeg", "su39.jpeg"]),
                ("Jeans", ["su41.jpeg", "su42.jpeg", "su43.jpeg", "su44.jpeg", "su45.jpeg", "su46.jpeg", "su47.jpeg", "su48.jpeg", "su49.jpeg"]),
                ("Skirts", ["su51.jpeg", "su52.jpeg", "su53.jpeg", "su54.jpeg", "su55.jpeg", "su56.jpeg", "su57.jpeg", "su58.jpeg", "su59.jpeg"])
            ]),
            ("Formals", [
                ("Suit", ["fo1.jpg", "fo2.jpg", "fo3.jpg", "fo4.jpg", "fo5.jpg", "fo6.jpg", "fo7.jpg", "fo8.jpg", "fo9.jpg"]),
                ("Pencil skirt", ["fo11.jpg", "fo12.jpg", "fo13.jpg", "fo14.jpg", "fo15.jpg", "fo16.jpg", "fo17.jpg", "fo18.jpg", "fo19.jpg"]),
                ("Jean", ["fo21.jpg", "fo22.jpg", "fo23.jpg", "fo24.jpg", "fo25.jpg", "fo26.jpg", "fo27.jpg", "fo28.jpg", "fo29.jpg"])
            ])
        ]

        for occasion, data in occasions_data:
            for button_name, images in data:
                for image_path in images:
                    c.execute("INSERT INTO new_hairstyles (occasion, button_name, image) VALUES (?, ?, ?)", (occasion, button_name, image_path))

        conn.commit()
        conn.close()
    except sqlite3.Error as e:
        print("SQLite error:", e)


def fetch_images(occasion, dress_type=""):
    try:
        conn = sqlite3.connect('new_haistyles.db')  # Corrected database name
        c = conn.cursor()
        if dress_type:
            c.execute("SELECT image FROM new_hairstyles WHERE occasion=? AND button_name=?", (occasion, dress_type))
        else:
            c.execute("SELECT image FROM new_hairstyles WHERE occasion=?", (occasion,))
        image_paths = c.fetchall()
        conn.close()
        return [row[0] for row in image_paths]  # Extracting image paths from the fetched rows
    except sqlite3.Error as e:
        print("SQLite error:", e)
        return []

def open_occasion_page(prev_window, occasion):
    prev_window.withdraw()
    occasion_window = tk.Toplevel()
    occasion_window.geometry('800x550+550+200')
    occasion_window.title('Dress Types')
    occasion_window.resizable(False, False)
    occasion_window.iconbitmap('your_icon.ico')
    
    background_image = Image.open('welcome.jpg')
    background_image = background_image.resize((800, 550))  
    background_photo = ImageTk.PhotoImage(background_image)
    background_label = tk.Label(occasion_window, image=background_photo)
    background_label.image = background_photo 
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    
    occasion_label = tk.Label(occasion_window, text=f"Dress types for {occasion}:", font=('Verdana', 18, 'bold'))
    occasion_label.pack(pady=60)

    dress_types = []
    if occasion == "Wedding":
        dress_types = ["Saree", "Lehenga", "Half saree", "Long frock", "Anarkali"]
    elif occasion == "Get Togethers":
        dress_types = ["Jeans", "Simple outfits", "Flowy maxi dress", "Layered", "Jumpsuit"]
    elif occasion == "Partys":
        dress_types = ["Cocktail dress", "Metallic dress", "Jeans"]
    elif occasion == "Festivals":
        dress_types = ["Long frock", "Jeans", "Anarkali", "Kurtis", "Saree"]
    elif occasion == "Casual Wear":
        dress_types = ["Jeans", "Kurtis", "Dresses"]
    elif occasion == "Sports":
        dress_types = ["Shorts", "Leggings", "Training outfits"]
    elif occasion == "Summer Styles":
        dress_types = ["Jump suit", "Frocks", "Flowy dresses", "Maxi floral dresses", "Jeans", "Skirts"]
    elif occasion == "Formals":
        dress_types = ["Suit", "Pencil skirt", "Jean"]

    button_frame = tk.Frame(occasion_window)
    button_frame.pack(pady=10)

    for dress_type in dress_types:
        dress_btn = tk.Button(button_frame, text=dress_type, font=('Verdana', 10), bg='lightblue', command=lambda dt=dress_type: open_dress_page(occasion_window, occasion, dt),width=35,height=2)
        dress_btn.pack(fill='x', padx=10, pady=5)
    
    back_btn = tk.Button(occasion_window, text="Back", command=lambda: [occasion_window.destroy(), prev_window.deiconify()],width=13,height=2)
    back_btn.pack(pady=12)

    occasion_window.mainloop()


def open_dress_page(prev_window, occasion, dress_type):
    dress_window = tk.Toplevel()
    dress_window.title(f'{occasion} - {dress_type}')
    dress_window.geometry('800x550+550+200')
    dress_window.iconbitmap('your_icon.ico')
    dress_window.resizable(False, False)
    image_paths = fetch_images(occasion, dress_type)
    
    for row, img_path in enumerate(image_paths):
        if row == 9:
            break  # Break the loop after displaying the 9th picture
        img = Image.open(img_path)
        img = img.resize((240, 140), Image.LANCZOS)
        img = ImageTk.PhotoImage(img)
        img_label = tk.Label(dress_window, image=img)
        img_label.image = img
        img_label.grid(row=row // 3, column=row % 3, padx=10, pady=10)
        img_label.bind("<Button-1>", lambda event, path=img_path: display_steps(path, occasion, dress_type))  # Bind click event to display steps
    
    back_button = tk.Button(dress_window, text="Back", command=dress_window.destroy,width=13,height=2)
    back_button.grid(row=row // 3 + 1, column=1, padx=10, pady=10) 

    dress_window.mainloop()

import webbrowser

def display_steps(image_path, occasion, dress_type):
    # Here, you can implement logic to display steps for the selected hairstyle image
    # You can create a new window or dialog box to display the steps
    # For example:
    steps_window = tk.Toplevel()
    steps_window.title("Steps")
    steps_window.geometry("800x550+550+200")
    steps_window.iconbitmap('your_icon.ico')
    steps_window.resizable(False, False)  
    steps_label = tk.Label(steps_window, text=f"Steps to wear {dress_type} for {occasion}:", font=('Verdana', 12, 'bold'))
    steps_label.pack(pady=10)
    
    # Assuming you have predefined steps for each occasion and dress type
    steps = {
        ("Wedding", "Saree"): [
            "Step 1: Drape the saree around the waist",
            "Step 2: Make pleats and tuck in",
            "Step 3: Wrap the pallu over the shoulder",
            f"Step 4: Watch this video for detailed steps: <a href='https://www.youtube.com/watch?v=eGblUZD-lRI'>Click here</a>"
        ],
        ("Wedding", "Lehenga"): [
            "Step 1: Wear the blouse and petticoat",
            "Step 2: Put on the lehenga skirt and secure it",
            "Step 3: Arrange the dupatta",
            f"Step 4: Watch this video for detailed steps: <a href='https://www.youtube.com/watch?v=eGblUZD-lRI'>Click here</a>"
        ],
        # Add more steps for each occasion and dress type as needed
    }
    
    steps_for_image = steps.get((occasion, dress_type), ["Steps not available"])  # Get steps for the selected image
    
    for step in steps_for_image:
        if "Watch this video" in step:
            # Extract video link from the step text
            video_link = step.split("href='")[1].split("'>")[0]
            # Create a clickable link
            step_label = tk.Label(steps_window, text=step, font=('Verdana', 10), fg="blue", cursor="hand2")
            step_label.pack(anchor='w', padx=20, pady=5)
            # Open the video link in the web browser when clicked
            step_label.bind("<Button-1>", lambda e, link=video_link: webbrowser.open_new(link))
        else:
            step_label = tk.Label(steps_window, text=step, font=('Verdana', 10))
            step_label.pack(anchor='w', padx=20, pady=5)
    
    back_button = tk.Button(steps_window, text="Back", command=steps_window.destroy, width=10, height=2)
    back_button.pack(pady=10)


def open_next_window():
    root.withdraw() 
    next_window = tk.Toplevel()
    next_window.geometry('800x550+550+200')
    next_window.title('Occasion_window')
    next_window.resizable(False, False)
    next_window.iconbitmap('your_icon.ico')
    
    background_image = Image.open('welcome.jpg')
    background_image = background_image.resize((800, 550))  
    background_photo = ImageTk.PhotoImage(background_image)
    background_label = tk.Label(next_window, image=background_photo)
    background_label.image = background_photo 
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    
    welcome_label = tk.Label(next_window, text='Choose your Ocassion!!', font=('Helvetica', 14, 'italic'))
    welcome_label.pack(pady=10)
    

    user_frame1 = tk.Frame(next_window, width=800, height=300, bg='')
    user_frame1.pack(pady=20)


    occasions = ["Wedding", "Get Togethers", "Partys", "Festivals", "Casual Wear", "Sports", "Summer Styles", "Formals"]
    for occasion in occasions:
        user_btn = tk.Button(user_frame1, text=occasion, font=('Verdana', 10), bg='lightblue',
                             command=lambda o=occasion: open_occasion_page(next_window, o),width=35,height=2)
        user_btn.pack(fill='x', padx=20, pady=3)
    
    back_btn = tk.Button(next_window, text="Back", command=lambda: [next_window.destroy(), root.deiconify()],width=13,height=2)
    back_btn.place(x=350,y=500)

initialize_database()

root = tk.Tk()
root.geometry('800x550+550+200')
root.title('HAIRSTYLE')
root.iconbitmap('your_icon.ico')
user_frame = tk.Frame(root, width=350, height=350)
user_frame.pack()
root.resizable(False, False)

user_logo = Image.open('welcome.jpg')
user_logo = user_logo.resize((800, 550))
user_logo = ImageTk.PhotoImage(user_logo)
user_logo_lbl = tk.Label(root, image=user_logo)
user_logo_lbl.place(x=0, y=0)

user_name = tk.Label(root, text='Welcome To Caiffure!!', font=('Helvetica', 38, 'bold italic'))
user_name.place(x=150, y=170)

user_btn = tk.Button(root, text='Get Started', font=('Arial', 20), bg='lightgreen', command=open_next_window,width=15,height=1)
user_btn.place(x=280, y=300)

root.mainloop()


